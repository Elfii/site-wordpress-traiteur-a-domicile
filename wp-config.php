<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'KgasnUUY`sP!g3!qF*SZ/WGxm<:S~rPwWR~OzVq5_b~y_m83Xg!Kf^6?i_45]-T}' );
define( 'SECURE_AUTH_KEY',  'H#5?5Ne3K>izfc1`H:WFbDI8N6wc;F,Ek=,vvAzf1C|]MehC/$dKQZI=a4FJm#!Y' );
define( 'LOGGED_IN_KEY',    '^Y kH=t)_&IN$I|W?SN2k#AWGh`*dP6_@-zJBbbKzVeov6<QoJm5|DS.^P[*Ap86' );
define( 'NONCE_KEY',        '!.bO6CA7XQzuB`bPtjlcuud#px^_bMh.m`0!wxziU#2i0RI-!E]k{zPfXR%gZvtf' );
define( 'AUTH_SALT',        '@T,y @#hMj4^Swoc1f#ao=Up<wZ=Pw+qxk]N?HO)3%L/aPGlp8DZu*j4@<5`r#<%' );
define( 'SECURE_AUTH_SALT', '{.::?kC XCWBLc!a$fI=6C,fjE!)v}_vgP@|xz}<D.Sm1GD}%XdaM:VN@DPKfHkZ' );
define( 'LOGGED_IN_SALT',   'Z8S$h{KH<uiAP<5[L{m~FrZyz2.RD5i}L#Y3}FEpvzOknGy26uQaqn$}zU&]z<yq' );
define( 'NONCE_SALT',       'yQ4isz]):QA[RXFvsjO0&>wC0uEt r0HPewa)yN3+Ddf -{!&,3O+SQ !(.zW[S/' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
